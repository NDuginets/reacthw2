import React, { Component } from 'react';
import Toggle from './Toggle';


class FormComponent extends Component {

    state = {
        Argument: 'nothing'
    };

    changeState = (event) => {
        let newArgument = event.target.id;
        this.setState({Argument:newArgument})
    };
  

      render() {
    
        return (
          <div >
            <p>I`m state of FormComponent and my state.Argument={this.state.Argument}</p>
            <Toggle  changeState={this.changeState}/>
          </div>
        );
      }
    }
    
    
export default FormComponent;