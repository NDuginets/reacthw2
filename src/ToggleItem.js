import React, { Component } from 'react';



class ToggleItem extends Component {

      render() {
          let {name, changeState} = this.props
        
          return (
            <div className="toggle">
               <input  type="radio" id={name} name="forState" onChange={changeState}/>
               <label >{name}</label>
            </div>
          );
        }
      }
      
      
  export default ToggleItem;