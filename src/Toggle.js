import React, { Component } from 'react';
import ToggleItem from './ToggleItem';



class Toggle extends Component {

          render() {
            let {changeState} = this.props;
            return (
              <div >
                <ToggleItem name={'left'} changeState={changeState}/>
                <ToggleItem name={'midle'} changeState={changeState}/>
                <ToggleItem name={'right'} changeState={changeState}/>
              </div>
            );
          }
        }
        
        
export default Toggle;